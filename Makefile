SHELL = /bin/bash
.SHELLFLAGS=-o errexit -c

IMAGE_ORGANISATION=camdl
IMAGE_NAME=docker-compose-pipelines

DOCKER_IMAGE_NAME = $(IMAGE_ORGANISATION)/$(IMAGE_NAME)
COMMIT_TAG = $(shell git describe --exact-match HEAD 2>/dev/null)
COMMIT_SHORT_HASH = $(shell git rev-parse --short=4 HEAD)
COMMIT_FULL_HASH = $(shell git rev-parse HEAD)

ensure-clean-checkout:
# Refuse to build a package with local modifications, as the package may end up
# containing the modifications rather than the committed state.
	@DIRTY_FILES="$$(git status --porcelain)" ; \
	if [ "$$DIRTY_FILES" != "" ]; then \
		echo "Error: git repo has uncommitted changes, refusing to build as the result may not be reproducible" ; \
		echo "$$DIRTY_FILES" ; \
		exit 1 ; \
	fi

build: ensure-clean-checkout
	docker build \
		$(if $(COMMIT_TAG), --tag "$(DOCKER_IMAGE_NAME):$(COMMIT_TAG)" --build-arg COMMIT_TAG="$(COMMIT_TAG)") \
		--tag "$(DOCKER_IMAGE_NAME):$(COMMIT_SHORT_HASH)" \
		--build-arg "COMMIT_FULL_HASH=$(COMMIT_FULL_HASH)" .

publish: publish-commit-hash publish-tagged-version

publish-commit-hash: build
	docker image push $(DOCKER_IMAGE_NAME):$(COMMIT_SHORT_HASH)

publish-tagged-version: build
	if [[ "$(COMMIT_TAG)" == "" ]]; then \
		echo "Skipping push for non-tagged image"; \
	else \
		docker image push $(DOCKER_IMAGE_NAME):$(COMMIT_TAG); \
	fi

.PHONY: build publish publish-commit-hash publish-tagged-version
