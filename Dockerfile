FROM alpine:3.13.1 as base

# This stage installs docker-compose from pypi, as the latest versions are
# there. The Alpine package repos contain only older versions.
FROM base as build

RUN apk add --no-cache \
  py3-pip build-base python3-dev libressl-dev musl-dev libffi-dev
RUN pip install docker-compose==1.28.2


FROM base as main

ARG COMMIT_TAG
ARG COMMIT_FULL_HASH

RUN '[' "${COMMIT_FULL_HASH}" != "" ] || (echo 'Error: COMMIT_FULL_HASH build arg is not set'; exit 1)

LABEL org.opencontainers.image.title="Docker Compose Pipelines"
LABEL org.opencontainers.image.description="A base image for Bitbucket Pipelines CI jobs that invoke docker-compose commands."
LABEL org.opencontainers.image.url="https://bitbucket.org/CUDL/docker-compose-pipelines"
LABEL org.opencontainers.image.source="https://bitbucket.org/CUDL/docker-compose-pipelines"
LABEL org.opencontainers.image.version=$COMMIT_TAG
LABEL org.opencontainers.image.revision=$COMMIT_FULL_HASH
LABEL maintainer="https://bitbucket.org/CUDL/"

# Packages we install:
#  - docker-cli: So that we can run regular docker commands. Note that the
#      daemon is not required, because bitbucket provides it via a pipelines
#      service.
# - docker-compose (from build stage): So that we can run our docker-compose stuff.
# - bash: Pipelines script steps are executed with bash
# - git: Because it's useful to be able to use git at the top level, e.g. to
#     calculate image tags from git revisions.
# - grep: Alpine has a grep command, but does less than GNU grep
# - openssh-client: Required to clone git repos via ssh
RUN apk add --no-cache \
  docker-cli=20.10.2-r0 \
  python3 bash git grep openssh-client && \
# The Bitbucket Pipelines environment mounts its own docker binary at
# /usr/bin/docker which would override the one we install. /usr/local/bin is
# ahead on the $PATH, so putting ours there gives it priority.
  mv /usr/bin/docker /usr/local/bin/

COPY --from=build /usr/lib/python3.8/site-packages/ /usr/lib/python3.8/site-packages/
COPY --from=build /usr/bin/docker-compose /usr/bin/docker-compose

# Run docker_envars.sh when bash runs (interactively). Pipelines steps are run
# interactively, so this occurs before each step.
COPY ./docker_envars.sh /root/docker_envars.sh
RUN printf "\n. /root/docker_envars.sh" >> /root/.bashrc
