# `docker-compose` pipelines

This is a Docker image to run [`docker-compose`][docker-compose] commands in the Bitbucket Pipelines CI environment.

## Versions

Images are available on Dockerhub at `camdl/docker-compose-pipelines:$TAG`.

`$TAG` values are the git tags in this repo, and short versions of git commit hashes. See https://hub.docker.com/repository/docker/camdl/docker-compose-pipelines/

## Usage

Assuming you have a `docker-compose.yml` file in your project, you can use the image as follows.

`bitbucket-pipelines.yml`:
```yaml
image: camdl/docker-compose-pipelines:1.0.0

pipelines:
  default:
    - step:
        name: Build something defined in docker-compose.yml
        script:
          - docker-compose run build
        services:
          - docker
        caches:
          - docker
definitions:
  services:
    docker:
      memory: 3072
```

```yaml
version: "3"

services:
  build:
    build:
      context: .
      dockerfile: docker/build.Dockerfile
    environment:
      DOCKER_HOST: ${DOCKER_HOST:-unix:///var/run/docker.sock}
    volumes:
      # Provide access to our docker daemon to the build container so that it
      # can use it to build the image. Note: this is only for local runs, under
      # Bitbucket Pipelines DOCKER_HOST specifies a tcp:// URL for the docker
      # daemon.
      - /var/run/docker.sock:/var/run/docker.sock
      - .:/code
    command: make docker-image
```

Note that `$DOCKER_HOST` must be propagated to docker-compose containers that need to invoke docker commands. They'll use the top-level `docker` service provided by the Pipelines build - the same one used by the `docker-compose` commands in the `bitbucket-pipelines.yml` file.

## Known issues

- Pipelines does not support the buildkit image builder:
    - https://community.atlassian.com/t5/Bitbucket-Pipelines-questions/Docker-BuildKit-support/qaq-p/1315659
    - https://jira.atlassian.com/browse/BCLOUD-17590
    - It also doesn't support the `--privileged` run option, so using docker-in-docker is not an option to work around this.

## Development

The `Makefile` is used to build the image with the right build vars:

```console
$ make build
```

### Releasing a version

To release, tag your change commit with the version tag (must be annotated), then use make to publish:

```console
$ git tag -a 3.4.5 -m 3.4.5 HEAD
$ git push origin 3.4.5
$ make publish
```

### Testing a version

To test new versions in Pipelines they need to be public on Dockerhub. Either just push the commit hash tag:

```console
$ make publish-commit-hash
```

Or use a pre-release version:

```console
$ git tag -a 3.4.5-test.0 -m 3.4.5-test.0 HEAD
$ make publish
```

## See Also

* [Using Docker-in-Docker for your CI or testing environment? Think twice.][ci-docker-in-docker] — A blog post on why it's desirable to have nested docker builds use a common daemon rather than Docker-in-Docker

[docker-compose]: https://docs.docker.com/compose/
[ci-docker-in-docker]: https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/
