# Bitbucket provides a docker daemon to CI builds when using their `docker`
# service. We want to be able to share this daemon with containers run by
# docker-compose, rather than doing docker-in-docker. See:
# https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/
#
# The docker daemon provided by the `docker` service is on localhost listening
# on a TCP port (not the common UNIX socket).
#
# Containers running on this daemon are able to access localhost via its actual
# IP, so we can share the daemon with containers we start by providing them with
# our actual IP. The $BITBUCKET_DOCKER_HOST_INTERNAL envar is the IP of the host
# running the current pipelines CI job. See:
# https://community.atlassian.com/t5/Bitbucket-articles/Changes-to-make-your-containers-more-secure-on-Bitbucket/ba-p/998464
#
# The DOCKER_HOST envar by default points to localhost; we rewrite it to point
# to our actual IP address, so that it works from within containers running on
# our docker daemon.
if (echo "$DOCKER_HOST" | grep -Pq '^tcp://localhost:[0-9]+$' && [ "$BITBUCKET_DOCKER_HOST_INTERNAL" != "" ])
then
  export DOCKER_HOST="tcp://$BITBUCKET_DOCKER_HOST_INTERNAL:$(echo "$DOCKER_HOST" | grep -oP '[0-9]+$')"
fi
